/**
* @ignore
*/
declare const config: {
    network: import("./network").BlockstackNetwork;
    logLevel: string;
};
export { config };
//# sourceMappingURL=config.d.ts.map