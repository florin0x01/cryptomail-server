/**
 * @ignore
 */
export declare const BLOCKSTACK_GAIA_HUB_LABEL = "blockstack-gaia-hub-config";
/**
 * The configuration for the user's Gaia storage provider.
 */
export interface GaiaHubConfig {
    address: string;
    url_prefix: string;
    token: string;
    server: string;
}
/**
 *
 * @param filename
 * @param contents
 * @param hubConfig
 * @param contentType
 *
 * @ignore
 */
export declare function uploadToGaiaHub(filename: string, contents: any, hubConfig: GaiaHubConfig, contentType?: string): Promise<string>;
export declare function deleteFromGaiaHub(filename: string, hubConfig: GaiaHubConfig): Promise<void>;
/**
 *
 * @param filename
 * @param hubConfig
 *
 * @ignore
 */
export declare function getFullReadUrl(filename: string, hubConfig: GaiaHubConfig): Promise<string>;
/**
 *
 * @ignore
 */
export declare function connectToGaiaHub(gaiaHubUrl: string, challengeSignerHex: string, associationToken?: string): Promise<GaiaHubConfig>;
/**
 *
 * @param gaiaHubUrl
 * @param appPrivateKey
 *
 * @ignore
 */
export declare function getBucketUrl(gaiaHubUrl: string, appPrivateKey: string): Promise<string>;
//# sourceMappingURL=hub.d.ts.map