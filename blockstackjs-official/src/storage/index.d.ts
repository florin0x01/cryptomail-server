/// <reference types="node" />
import { connectToGaiaHub, uploadToGaiaHub, BLOCKSTACK_GAIA_HUB_LABEL } from './hub';
import { UserSession } from '../auth/userSession';
/**
 * Specify a valid MIME type, encryption, and whether to sign the [[UserSession.putFile]].
 */
export interface PutFileOptions {
    /**
    * Encrypt the data with the app public key.
    * If a string is specified, it is used as the public key.
    * If the boolean `true` is specified then the current user's app public key is used.
     * @default true
     */
    encrypt?: boolean | string;
    /**
     * Sign the data using ECDSA on SHA256 hashes with the user's app private key.
     * If a string is specified, it is used as the private key.
     * @default false
     */
    sign?: boolean | string;
    /**
     * Set a Content-Type header for unencrypted data.
     */
    contentType?: string;
    /**
     * if a Gaia Config is supplied, use that and override
     */
    gaiaConfig?: null;
}
/**
 * Fetch the public read URL of a user file for the specified app.
 * @param {String} path - the path to the file to read
 * @param {String} username - The Blockstack ID of the user to look up
 * @param {String} appOrigin - The app origin
 * @param {String} [zoneFileLookupURL=null] - The URL
 * to use for zonefile lookup. If falsey, this will use the
 * blockstack.js's [[getNameInfo]] function instead.
 * @return {Promise<string>} that resolves to the public read URL of the file
 * or rejects with an error
 */
export declare function getUserAppFileUrl(path: string, username: string, appOrigin: string, zoneFileLookupURL?: string): Promise<string | null>;
/**
 *
 *
 * @deprecated
 * #### v19 Use [[UserSession.encryptContent]].
 *
 * Encrypts the data provided with the app public key.
 * @param {String|Buffer} content - data to encrypt
 * @param {Object} [options=null] - options object
 * @param {String} options.publicKey - the hex string of the ECDSA public
 * key to use for encryption. If not provided, will use user's appPublicKey.
 * @return {String} Stringified ciphertext object
 */
export declare function encryptContent(content: string | Buffer, options?: {
    publicKey?: string;
}, caller?: UserSession): string;
/**
 *
 * @deprecated
 * #### v19 Use [[UserSession.decryptContent]].
 *
 * Decrypts data encrypted with `encryptContent` with the
 * transit private key.
 * @param {String|Buffer} content - encrypted content.
 * @param {Object} [options=null] - options object
 * @param {String} options.privateKey - the hex string of the ECDSA private
 * key to use for decryption. If not provided, will use user's appPrivateKey.
 * @return {String|Buffer} decrypted content.
 */
export declare function decryptContent(content: string, options?: {
    privateKey?: string;
}, caller?: UserSession): string | Buffer;
/**
 * @deprecated
 * #### v19 Use [[UserSession.getFileUrl]] instead.
 *
 * @param {String} path - the path to the file to read
 * @returns {Promise<string>} that resolves to the URL or rejects with an error
 */
export declare function getFileUrl(path: string, options?: GetFileUrlOptions, caller?: UserSession): Promise<string>;
export interface GetFileUrlOptions {
    /**
     * The Blockstack ID to lookup for multi-player storage.
     * If not specified, the currently signed in username is used.
     */
    username?: string;
    /**
     * The app to lookup for multi-player storage - defaults to current origin.
     * @default `window.location.origin`
     * Only if available in the executing environment, otherwise `undefined`.
     */
    app?: string;
    /**
     * The URL to use for zonefile lookup. If falsey, this will use
     * the blockstack.js's [[getNameInfo]] function instead.
     */
    zoneFileLookupURL?: string;
}
/**
 * Used to pass options to [[UserSession.getFile]]
 */
export interface GetFileOptions extends GetFileUrlOptions {
    /**
     * Try to decrypt the data with the app private key.
     * @default true
     */
    decrypt?: boolean;
    /**
     * Whether the content should be verified, only to be used
     * when [[UserSession.putFile]] was set to `sign = true`.
     * @default false
     */
    verify?: boolean;
}
/**
 * Retrieves the specified file from the app's data store.
 * @param {String} path - the path to the file to read
 * @returns {Promise} that resolves to the raw data in the file
 * or rejects with an error
 */
export declare function getFile(path: string, options?: GetFileOptions, caller?: UserSession): Promise<string | ArrayBuffer>;
/**
 * Stores the data provided in the app's data store to to the file specified.
 * @param {String} path - the path to store the data in
 * @param {String|Buffer} content - the data to store in the file
 * @return {Promise} that resolves if the operation succeed and rejects
 * if it failed
 */
export declare function putFile(path: string, content: string | Buffer, options?: PutFileOptions, caller?: UserSession): Promise<string>;
/**
 * Deletes the specified file from the app's data store.
 * @param path - The path to the file to delete.
 * @param options - Optional options object.
 * @param options.wasSigned - Set to true if the file was originally signed
 * in order for the corresponding signature file to also be deleted.
 * @returns Resolves when the file has been removed or rejects with an error.
 */
export declare function deleteFile(path: string, options?: {
    wasSigned?: boolean;
}, caller?: UserSession): Promise<void>;
/**
 * Get the app storage bucket URL
 * @param {String} gaiaHubUrl - the gaia hub URL
 * @param {String} appPrivateKey - the app private key used to generate the app address
 * @returns {Promise} That resolves to the URL of the app index file
 * or rejects if it fails
 */
export declare function getAppBucketUrl(gaiaHubUrl: string, appPrivateKey: string): Promise<string>;
/**
 * List the set of files in this application's Gaia storage bucket.
 * @param {function} callback - a callback to invoke on each named file that
 * returns `true` to continue the listing operation or `false` to end it
 * @return {Promise} that resolves to the number of files listed
 */
export declare function listFiles(callback: (name: string) => boolean, caller?: UserSession): Promise<number>;
export { connectToGaiaHub, uploadToGaiaHub, BLOCKSTACK_GAIA_HUB_LABEL };
//# sourceMappingURL=index.d.ts.map