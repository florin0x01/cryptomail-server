import { ECPair } from 'bitcoinjs-lib';
/**
 *  @ignore
 */
export declare const BLOCKSTACK_HANDLER = "blockstack";
/**
 * Time
 * @private
 * @ignore
 */
export declare function nextYear(): Date;
/**
 * Time
 * @private
 * @ignore
 */
export declare function nextMonth(): Date;
/**
 * Time
 * @private
 * @ignore
 */
export declare function nextHour(): Date;
/**
 * Query Strings
 * @private
 * @ignore
 */
export declare function updateQueryStringParameter(uri: string, key: string, value: string): string;
/**
 * Versioning
 * @param {string} v1 - the left half of the version inequality
 * @param {string} v2 - right half of the version inequality
 * @returns {bool} iff v1 >= v2
 * @private
 * @ignore
 */
export declare function isLaterVersion(v1: string, v2: string): boolean;
/**
 * Time
 * @private
 * @ignore
 */
export declare function hexStringToECPair(skHex: string): ECPair.ECPairInterface;
/**
 *
 * @ignore
 */
export declare function ecPairToHexString(secretKey: ECPair.ECPairInterface): string;
/**
 * Time
 * @private
 * @ignore
 */
export declare function ecPairToAddress(keyPair: ECPair.ECPairInterface): string;
/**
 * UUIDs
 * @private
 * @ignore
 */
export declare function makeUUID4(): string;
/**
 * Checks if both urls pass the same origin check & are absolute
 * @param  {[type]}  uri1 first uri to check
 * @param  {[type]}  uri2 second uri to check
 * @return {Boolean} true if they pass the same origin check
 * @private
 * @ignore
 */
export declare function isSameOriginAbsoluteUrl(uri1: string, uri2: string): boolean;
interface GetGlobalObjectOptions {
    /**
     * Throw an error if the object is not found.
     * @default false
     */
    throwIfUnavailable?: boolean;
    /**
     * Additional information to include in an error if thrown.
     */
    usageDesc?: string;
    /**
     * If the object is not found, return an new empty object instead of undefined.
     * Requires [[throwIfUnavailable]] to be falsey.
     * @default false
     */
    returnEmptyObject?: boolean;
}
/**
 * Returns an object from the global scope (`Window` or `WorkerGlobalScope`) if it
 * is available within the currently executing environment.
 * When executing within the Node.js runtime these APIs are unavailable and will be
 * `undefined` unless the API is provided via polyfill.
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Window/self
 * @ignore
 */
export declare function getGlobalObject<K extends keyof Window>(name: K, { throwIfUnavailable, usageDesc, returnEmptyObject }?: GetGlobalObjectOptions): Window[K];
/**
 * Returns a specified subset of objects from the global scope (`Window` or `WorkerGlobalScope`)
 * if they are available within the currently executing environment.
 * When executing within the Node.js runtime these APIs are unavailable will be `undefined`
 * unless the API is provided via polyfill.
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Window/self
 * @ignore
 */
export declare function getGlobalObjects<K extends keyof Window>(names: K[], { throwIfUnavailable, usageDesc, returnEmptyObject }?: GetGlobalObjectOptions): Pick<Window, K>;
export {};
//# sourceMappingURL=utils.d.ts.map