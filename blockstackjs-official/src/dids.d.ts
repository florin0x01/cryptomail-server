/**
* @ignore
*/
export declare function makeDIDFromAddress(address: string): string;
/**
* @ignore
*/
export declare function makeDIDFromPublicKey(publicKey: string): string;
/**
* @ignore
*/
export declare function getDIDType(decentralizedID: string): string;
/**
* @ignore
*/
export declare function getAddressFromDID(decentralizedID: string): string;
//# sourceMappingURL=dids.d.ts.map