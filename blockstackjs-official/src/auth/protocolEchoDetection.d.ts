/**
 * This logic is in a separate file with no dependencies so that it can be
 * loaded and executed as soon as possible to fulfill the purpose of the protocol
 * detection technique. The effectiveness of this is obviously subject to how web
 * apps bundle/consume the blockstack.js lib.
 */
/**
 * Checks if the current window location URL contains an 'echoReply' parameter
 * which indicates that this page was only opened to signal back to the originating
 * tab that the protocol handler is installed.
 * If found, then localStorage events are used to notify the other tab,
 * and this page is redirected back to the Blockstack authenticator URL.
 * This function caches its result and will not trigger multiple redirects when
 * invoked multiple times.
 * @returns True if detected and the page will be automatically redirected.
 * @hidden
 */
export declare function protocolEchoReplyDetection(): boolean;
//# sourceMappingURL=protocolEchoDetection.d.ts.map