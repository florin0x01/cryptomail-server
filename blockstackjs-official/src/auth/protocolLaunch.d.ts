/**
 * Detects if the native auth-browser is installed and is successfully
 * launched via a custom protocol URI.
 * @param {String} authRequest
 * The encoded authRequest to be used as a query param in the custom URI.
 * @param {String} successCallback
 * The callback that is invoked when the protocol handler was detected.
 * @param {String} failCallback
 * The callback that is invoked when the protocol handler was not detected.
 * @return {void}
 */
export declare function launchCustomProtocol(authRequest: string, successCallback: () => void, failCallback: () => void): void;
//# sourceMappingURL=protocolLaunch.d.ts.map