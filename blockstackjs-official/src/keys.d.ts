/// <reference types="node" />
/**
 *
 * @param numberOfBytes
 *
 * @ignore
 */
export declare function getEntropy(arg: Buffer | number): Buffer;
/**
* @ignore
*/
export declare function makeECPrivateKey(): string;
/**
* @ignore
*/
export declare function publicKeyToAddress(publicKey: string): string;
/**
* @ignore
*/
export declare function getPublicKeyFromPrivate(privateKey: string): string;
//# sourceMappingURL=keys.d.ts.map