export declare function getName(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getGivenName(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getFamilyName(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getDescription(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getAvatarUrl(profile: any): string;
/**
 *
 * @ignore
 */
export declare function getVerifiedAccounts(profile: any, verifications?: any[]): any[];
/**
 *
 * @ignore
 */
export declare function getOrganizations(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getConnections(profile: any): any;
/**
 *
 * @ignore
 */
export declare function getAddress(profile: any): string;
/**
 *
 * @ignore
 */
export declare function getBirthDate(profile: any): string;
//# sourceMappingURL=personUtils.d.ts.map