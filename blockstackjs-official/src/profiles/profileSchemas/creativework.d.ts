import { Profile } from '../profile';
/**
 * @ignore
 */
export declare class CreativeWork extends Profile {
    constructor(profile?: {});
    /**
     *
     * @ignore
     */
    static validateSchema(profile: any, strict?: boolean): any;
    /**
     * @ignore
     */
    static fromToken(token: string, publicKeyOrAddress?: string | null): CreativeWork;
}
//# sourceMappingURL=creativework.d.ts.map