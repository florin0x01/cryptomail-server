/**
 *
 * @param zoneFile
 * @param publicKeyOrAddress
 * @param callback
 *
 * @ignore
 */
export declare function resolveZoneFileToPerson(zoneFile: any, publicKeyOrAddress: string, callback: (profile: any) => void): void;
//# sourceMappingURL=personZoneFiles.d.ts.map