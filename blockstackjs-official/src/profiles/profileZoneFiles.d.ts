/**
 *
 * @param origin
 * @param tokenFileUrl
 *
 * @ignore
 */
export declare function makeProfileZoneFile(origin: string, tokenFileUrl: string): any;
/**
 *
 * @param zoneFileJson
 *
 * @ignore
 */
export declare function getTokenFileUrl(zoneFileJson: any): string | null;
/**
 *
 * @param zoneFile
 * @param publicKeyOrAddress
 *
 * @ignore
 */
export declare function resolveZoneFileToProfile(zoneFile: any, publicKeyOrAddress: string): Promise<{}>;
//# sourceMappingURL=profileZoneFiles.d.ts.map