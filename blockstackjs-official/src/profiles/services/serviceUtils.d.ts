/**
 *
 * @ignore
 */
export declare function containsValidProofStatement(searchText: string, name?: string | null): boolean;
/**
 *
 * @ignore
 */
export declare function containsValidAddressProofStatement(proofStatement: string, address: string): boolean;
//# sourceMappingURL=serviceUtils.d.ts.map