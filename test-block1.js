var fs = require('fs');
var streams = require('memory-streams');
const { blockstackObj  } = require('./blockstack-server')
const { readFile, putFile, getMetadata, putMetadata  } = require('./utils')
const { decodeToken, TokenVerifier }  = require('jsontokens')
const blockstack = require('blockstack')

// https://blockstack.github.io/blockstack.js/classes/usersession.html#loaduserdata

function doSmth() {
    var self = this
    blockstackObj().then((userData) => {
        var now = new Date()        
        var fsReader = new fs.ReadStream()
        var memoryWriter = new streams.WritableStream();

        fsReader.pipe(memoryWriter)
        memoryWriter.on('error', function(err) {
            self.logerror('Error saving file', err)
            return next(DENY, 'Error saving file')
        })

        fsReader.on('end', function() {
            var contents = memoryWriter.toString()
            blockstackObj.userSession.putFile('/mail/incoming/mail- ' + now.getTime(), contents, {
                encrypt: true
            }).then(res => console.log('Succ'))
            .catch(err => console.err(err))

            return next(OK);
        })
    })    
}

function getProfile() {
    blockstackObj().then(userData => {
        const tokenPayload = decodeToken(blockstackObj.fakeToken).payload

        console.log('TOK PAYLOAD DECODED ', tokenPayload)
        console.log('======== START loadUserData from blockstack ===========')

       const uData = blockstackObj.userSession.loadUserData()
        console.log(uData)
        console.log('**************')
        console.log( blockstack.getPublicKeyFromPrivate(uData.appPrivateKey))

        console.log('+++++++++++++++++++++++ UserData from callback ++++++++++++')
        console.log(userData)
        
    })
}

function upload() {
    putMetadata('/test/file1.dat', 'Hello', 'user1@user.com', 0, 0).then(status => {
        if (!status) {
            console.log('Fail putting')
        } else {
            console.log('ok')
        }
    })
}


getProfile()
