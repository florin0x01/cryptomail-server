const { Session } = require('./blockstack-server')
const { getLocalAccount } = require('./utils')

async function getBlockstack(email) {
    let account = await getLocalAccount(email)
    if (!account) {
        console.log('No acct')
        return
    }
    const { blockstackToken, gaiaConfig } = account
    console.log('GaiaConfig ', gaiaConfig)

   // console.log('TOKEN ', token)
     let bSession = await Session(email, blockstackToken)

    let res = await bSession.putFile('/tmp/something', 'Content', {
        encrypt: account.pubKey,
        sign: false,
        gaiaConfig
    })
   console.log(res)
}

getBlockstack('abc@cryptomail.softwaremirage.com')
