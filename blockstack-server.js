//const Window = require('window'); 
//const window = new Window()
const {window} = require('./window')
global.fetch = require("node-fetch");
const blockstack = require('./blockstack');
const appConfig = new blockstack.AppConfig()

async function Session(email, token) {
   // const window = new Window();

    try {
        if (!Session.userSession) {
            Session.userSession = {}
        }
        console.log('TOKEN ', token)
        if (!Session.userSession[email]) {
             Session.userSession[email] = new blockstack.UserSession({ appConfig: appConfig })
            // const transitPrivateKy = Session.userSession[email].generateAndStoreTransitKey()
            //window.localStorage.setItem('blockstack-transit-private-key', transitPrivateKy)
            // window['location']['search'] = `?authResponse=${token}`
            if (!Session.userSession[email].isUserSignedIn()) {
                await Session.userSession[email].handlePendingSignIn(token)
            }
        }
      
        return Promise.resolve(Session.userSession[email])
    }catch(err) {
        console.log('ERR: ', err)
        return null
    }
}

module.exports = { Session  }




