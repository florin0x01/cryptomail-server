const { blockstackObj  } = require('./blockstack-server')
const { TokenVerifier } = require('jsontokens')
var Redis = require("ioredis");
var redis = new Redis();

const CACHE_PREFIX = 'CRYPTOMAIL_'

function keyPrefix(key) {
    let myKey = CACHE_PREFIX + key
    if (key.indexOf(CACHE_PREFIX) === 0) {
      myKey = key
    }
    return myKey
  }
  
  async function cache_get(key) {
    let entry = await redis.get(keyPrefix(key))
    if (!entry) {
      //try old format, for the already established keys (with multiple prefix format)
      entry = await redis.get(key)
    }
    return entry
  }
  
  //TODO wrapper for transactions ?
  
  async function cache_set(key, value, expiry) {
    if (expiry) {
      return await redis.set(keyPrefix(key), value, 'EX', expiry)
    }
    return await redis.set(keyPrefix(key), value)
  }


  async function putFile(path, contents, userSession) {
        try {
            let data = await userSession.putFile(path, contents)
            return true
        }catch(err) {
            return err
        }
   }

async function putMetadata(mailheaderPath, subject, from, time,parentThreadId, session) {
    const payload = {
        parentThreadId,
        subject,
        from,
        time,
        to
    }
    let status = await putFile(mailheaderPath, JSON.stringify(payload), session)
    return status
}

function verifyBlockstackJWTToken(jsonToken, publicKey) {
    try {
        const tokenVerifier = new TokenVerifier('ES256k', publicKey)
        return tokenVerifier.verify(jsonToken)
    } catch(exception) {
        return false
    }
}

async function createLocalAccount(email, jsonToken, gaiaConfig, pubKey) {
    return await cache_set(email, JSON.stringify({blockstackToken: jsonToken, pubKey, gaiaConfig}))
}

async function getLocalAccount(email) {
    entry = await cache_get(email)
    if (entry) {
        entry = JSON.parse(entry)
        entry.gaiaConfig = JSON.parse(entry.gaiaConfig)
        return Promise.resolve(entry)
    }
    return null
}


module.exports = {
    putMetadata,
    verifyBlockstackJWTToken,
    createLocalAccount,
    getLocalAccount
}