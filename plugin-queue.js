var fs = require('fs');
var streams = require('memory-streams');
const simpleParser = require('mailparser').simpleParser;

const {         genIncomingMailName,
    genOutgoingMailName,
    genIncomingMailHeaderName 
} = require('./constants')
const { putFile, putMetadata  } = require('./utils')
const { Session } = require('./blockstack-server')

exports.hook_queue = function(next, connection, params) {
    var now = new Date()
    var memoryWriter = new streams.WritableStream()
    var self = this

    connection.transaction.message_stream.pipe(memoryWriter)
    memoryWriter.on('error', function(err) {
        self.logerror('Error saving file', err)
        return next(DENY, 'Error saving file')
    })

    connection.transaction.message_stream.on('end', function() {
        var contents = memoryWriter.toString()
        var ts = (new Date()).getTime()
        simpleParser(contents, undefined).then(
            parsed => {
                var To = parsed.to

                for(addr in To.value) {
                   var accountToken = getLocalAccount(addr.address)
                   if (!accountToken) {
                       continue
                   }
                    Session(addr.address, accountToken).then(function(uSession) {
                        var metaPath = genIncomingMailHeaderName(ts)
                        putMetadata(metaPath, parsed.subject, parsed.from.value[0].address, now.getTime(),0, uSession)
                            .then(metaStatus => {
                                if (!metaStatus) {
                                    throw new Error('Could not save meta')
                                }
                                var filePath = genIncomingMailName(ts)
                                putFile(filePath, contents).then(res => {
                                    if (res == true) {
                                        self.loginfo('Put file ', filePath)
                                        return next(OK)
                                    } else {
                                        self.logerror('Error putting file ', filePath)
                                        return next(DENY, 'Error saving file')
                                    }
                                })
                            })
                        
                    }).catch(function(errorFromBlockstackSession) {
                        self.logerror('Error getting blockstack session', errorFromBlockstackSession)
                        return next(DENY)
                    })
                }
            }
        ).catch(parseMailErr => {
            self.logerror('Error parsing mail ', parseMailErr)   
            return next(DENY)
        })
        return next(OK)
    })
}
