const crypto = require('crypto');

const localStorageRAM = {};
  
global['window'] = {
    location: {
        origin: 'localhost'
    },
    crypto: {
        getRandomValues: function(array) {
            return crypto.randomFillSync(array)
        }
    },
    localStorage: {
        getItem: function(itemName) {
            return localStorageRAM[itemName];
        },
        setItem: function(itemName, itemValue) {
            localStorageRAM[itemName] = itemValue;
        },
        removeItem: function(itemName) {
            delete localStorageRAM[itemName];
        }
    }
};

global['localStorage'] = global['window'].localStorage
module.exports = {
    window: global['window']
}