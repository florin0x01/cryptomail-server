const express = require('express')
var app = express()
var bodyParser = require('body-parser')
const asyncHandler = require('express-async-handler')
const utils = require('./utils')

app.use(bodyParser.json()) // for parsing application/json
function allowCrossDomain (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    next()
  }

app.use(allowCrossDomain)
app.post('/account/create', asyncHandler(async (req, res, next) => {
    let body = req.body
    
    if (!('email' in body)) {
      return res.status(400).json({error: 'No desired email in body'})
    }
    if (!('token' in body)) {
        return res.status(400).json({error: 'No token in body'})
    }
    if (!('gaiaConfig' in body)) {
        return res.status(400).json({error: 'No gaia config in body'})
    }

    if (!('publicKey' in body)) {
        return res.status(400).json({error: 'No public key in body'})
    }


    const desiredEmail = body.email
    const signedBlockstackToken = body.token
    const publicKey = body.publicKey
    const gaiaConfig = body.gaiaConfig
    
    console.log('Email ', desiredEmail)
    console.log('Token ', signedBlockstackToken)
    console.log('Public key ', publicKey)

    const signatureVerified = utils.verifyBlockstackJWTToken(signedBlockstackToken, publicKey)
   
    if (!signatureVerified) {
      return res.status(400).json({error: 'Could not verify signature of JSON token.'})
    } else {
        console.log('Verified ', signatureVerified)
    }

    const status =  await utils.createLocalAccount(desiredEmail, signedBlockstackToken, gaiaConfig, publicKey)
    console.log('KEY ', status)
    if (status != 'OK') {
      return res.status(500).json({error: 'No key could be created. Please contact app developer.'})
    }
    res.header("Content-Type", "application/json");
    res.status(200).json({status, email: desiredEmail})
    res.end()
}))

app.get('/account/:id')

app.listen(1025)


//For public key, which is transit : (authMessages.ts)
//   const publicKey = SECP256K1Client.derivePublicKey(transitPrivateKey)


/*

{
	"email": "a@a.com",
	"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NksifQ.eyJqdGkiOiI1YjQ2MmEzNS1lMWZiLTQ0MGQtOTVhNC1mYWJiOGQxZmRhMTEiLCJpYXQiOjE1NjkwODE1NzUsImV4cCI6MTU3Njg2MTE3NSwiaXNzIjoiZGlkOmJ0Yy1hZGRyOjFOWnBONkQ4dmJ5NVdwVWFpU2JHZDUzaW1DNlFmazJneU0iLCJwcml2YXRlX2tleSI6IjdiMjI2OTc2MjIzYTIyNjMzMjYzMzE2MzY1NjIzODMxNjMzMDM0MzI2NjYyMzMzNTMwNjQzNTM2MzEzOTY1MzQ2MTY2MzE2NTY0NjM2MTIyMmMyMjY1NzA2ODY1NmQ2NTcyNjE2YzUwNGIyMjNhMjIzMDMyMzI2NTM2MzA2MTY0MzYzNjYzMzEzODM3MzgzMDY1NjIzNDM0MzMzODMxNjIzMzY0MzkzMTY2MzU2MjYzNjMzMjMzNjQzNTMxMzc2NDY2MzY2NDM2MzA2MjM0MzIzMTMyMzUzNDYyMzMzNTMxMzk2NTY2MzMzNDMwMzYzMTY1MzQyMjJjMjI2MzY5NzA2ODY1NzI1NDY1Nzg3NDIyM2EyMjM1NjQ2NDY2NjQ2NjM4NjMzMzMwNjUzNTY1MzQzNDYyMzkzOTMxMzIzNDY1MzkzNjM2MzYzMzMzMzgzMTM2MzgzMTMyMzMzOTY2Mzg2MTY2MzAzNDYzMzczMDY1NjYzNTY0NjQzNTY2MzIzNzM4MzYzNDM2MzE2NDMzNjMzMjY1MzIzNDM4NjIzMjY1MzgzMDMyNjEzNzM2MzczOTM0NjQ2NDMxNjEzODY1Mzc2MjM4NjE2NTMwNjMzNjM2MzIzNDY0NjIzMDY0NjE2MjMxMzYzODM2MzkzNDYzMzU2NTY0MzkzMTYzNjIzMTM1NjQzMzMxNjY2NDY2MzYzOTMwNjQzODY0MzY2NDMzMzg2MjYyMzczMTY0NjI2MjM1NjEzNTM4Mzk2MzYyNjQ2NDY0Mzc2NTM4MzIzNTM3NjYzMDYyMjIyYzIyNmQ2MTYzMjIzYTIyNjQzMjY0MzAzMzYxMzE2NTYzNjIzNjMyMzEzNDY1MzIzNTM2MzAzNDY2NjUzOTM3MzQzNjMwMzk2NjMxMzIzNDY0MzgzNTY1MzYzMzYxMzQzNzM3NjMzMDYxNjEzOTMzNjMzOTYzMzgzMTYxMzQ2MjM2MzkzODMyNjMzOTMwNjIyMjJjMjI3NzYxNzM1Mzc0NzI2OTZlNjcyMjNhNzQ3Mjc1NjU3ZCIsInB1YmxpY19rZXlzIjpbIjAzMzdhNGFlZjFmODQyM2NhMDc2ZTRiN2Q5OWE4Y2FiZmY0MGRkYjgyMzFmMmE5ZjAxMDgxZjE1ZDdmYTY1YzFiYSJdLCJwcm9maWxlIjp7InR5cGUiOiJAUGVyc29uIiwiYWNjb3VudHMiOltdfSwidXNlcm5hbWUiOm51bGwsImNvcmVfdG9rZW4iOm51bGwsImVtYWlsIjpudWxsLCJwcm9maWxlX3VybCI6bnVsbCwiaHViVXJsIjpudWxsLCJibG9ja3N0YWNrQVBJVXJsIjpudWxsLCJhc3NvY2lhdGlvblRva2VuIjpudWxsLCJ2ZXJzaW9uIjoiMS4zLjEifQ.PK6h4WUM2jzLjnVShs9ajYiJTo82JWHdG58pVmyUFo3S_jiEE7eDG3_FOcaG6AQ7BYKSYGhcQdLNkfcXZcSsiA",
	"publicKey": "0337a4aef1f8423ca076e4b7d99a8cabff40ddb8231f2a9f01081f15d7fa65c1ba"
}


*/
