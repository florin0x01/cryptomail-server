const MAIL_FILE_INCOMING = '/cryptomail/inbox/mail'
const MAIL_FILE_OUTGOING = '/cryptomail/sent/mail'
const MAIL_HEADER_FILE_INCOMING = '/cryptomail/inbox/header'
const MAIL_HEADER_FILE_OUTGOING = '/cryptomail/sent/header'
const APP_PATH_EMAIL = '/cryptomail/config/email'

function genIncomingMailName() {
    var now = new Date()
    return MAIL_FILE_INCOMING + '-' + now.getTime()
}

function genOutgoingMailName(ts) {
    var _ts = ts;
    if (!_ts) {
        var now = new Date()
        _ts = now.getTime()
    }
    return MAIL_FILE_OUTGOING + '-' + _ts
}

function genIncomingMailHeaderName(ts) {
    var _ts = ts;
    if (!_ts) {
        var now = new Date()
        _ts = now.getTime()
    }
    return MAIL_HEADER_FILE_INCOMING + '-' + _ts
}

module.exports = {
    MAIL_FILE_INCOMING,
    MAIL_FILE_OUTGOING,
    MAIL_HEADER_FILE_INCOMING,
    MAIL_HEADER_FILE_OUTGOING,
    APP_PATH_EMAIL,
    genIncomingMailName,
    genOutgoingMailName,
    genIncomingMailHeaderName
}